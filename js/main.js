(function($) {
	"use strict";
	$(window).load(function() {
		$("#loader").fadeOut("slow");
	});
    $('.button-collapse').sideNav();
    $('.slider').slider({full_width: true,indicators:false});
    $('.parallax').parallax();
})(jQuery);
